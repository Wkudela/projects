# !/bin/sh

sm=small
med=medium
l=large

sm=$1$sm
med=$1$med
l=$1$l

./thor.py -v -p 1 -r 10 $sm 2>&1 > /dev/null | grep Average | awk '{pring $6}' >> smalllatency.dat

./thor.py -v -p 1 -r 10 $med 2>&1 > /dev/null | grep Average | awk '{pring $6}' >> mediumlatency.dat

./thor.py -v -p 1 -r 10 $l 2>&1 > /dev/null | grep Average | awk '{pring $6}' >> largelatency.dat

