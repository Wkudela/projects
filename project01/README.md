Project 01: Networking
======================

William Kudela
wkudela

I have finished my implementations of thor.py and spidey.py as well as scripts
to run the experiments, however the programs are not functioning correctly, hence no results and no report.

Please see the [networking project] write-up.

[networking project]: https://www3.nd.edu/~pbui/teaching/cse.20189.sp16/homework09.html
