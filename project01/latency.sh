#!/bin/sh

#Global Variables
val1=hello.html
val2=cgi-bin/env.sh
statf=$1$val1			#/www/hello.html
scrptf=$1$val2			#/www/cgi-bin/env.sh

#Testing w/o forking

./thor.py -v -r 10 -p 10 $1 2>&1 | grep Average | awk '{print $6}' >> directory.dat

./thor.py -v -r 10 -p 10 $statf 2>&1 | grep Average| awk '{print $6}' >> static.dat

./thor.py -v -r 10 -p 10 $scrptf 2>&1 | grep Average | awk '{print $ 6}' >> script.dat

#Testing w/ forking

./thor.py -f -v -r 10 -p 10 $1 2>&1 | grep Average | awk '{print $6}' >> directoryforked.dat

./thor.py -f -v -r 10 -p 10 $statf 2>&1 | grep Average | awk '{print $6}' >> staticforked.dat

./thor.py -f -v -r 10 -p 10 $scrptf 2>&1 | grep Average | awk '{pring $6}' >> scriptforked.dat
