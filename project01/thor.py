#!/usr/bin/env python2.7

import getopt
import logging
import os
import socket
import sys

# Constants

ADDRESS  = 'example.com'
PORT     = 80
PROGRAM  = os.path.basename(sys.argv[0])
LOGLEVEL = logging.INFO
processes = 1
requests = 1

# Utility Functions

def usage(exit_code=0):
	print >>sys.stderr, '''Usage: {program} [-r REQUESTS -p PROCESSES -v] URL

Options:

	-h		Show this help message
	-v 		Set logging to DEBUG level

	-r REQUESTS	Number of requests per process (default is 1)
	-p PROCESSES	Number of processes  (defualt is 1)
	'''.format(port=PORT, program=PROGRAM)
	
	sys.exit(exit_code)

# TCPClient Class

class TCPClient(object):

	def __init__(self, address=ADDRESS, port=PORT):
		''' Construct TCPClient object with the specified address and port '''
		self.logger  = logging.getLogger()                              # Grab logging instance
		self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)# Allocate TCP socket
		self.address = address                                          # Store address to listen on
		self.port    = port                                             # Store port to lisen on

	def handle(self):
		''' Handle connection '''
		self.logger.debug('Handle')
		raise NotImplementedError

	def run(self):
		''' Run client by connecting to specified address and port and then
		executing the handle method '''
		try:
			# Connect to server with specified address and port, create file object
			if(self.port):
				self.socket.connect((self.address, self.port))
			else:
				try:
					self.socket.connect((self.address, PORT))
				except Exception as e:
					self.logger.exception('Execption {}'.format(e))
			self.stream = self.socket.makefile('w+')
		except socket.error as e:
			self.logger.error('Could not connect to {}:{}: {}'.format(self.address, self.port, e))
			sys.exit(1)

		self.logger.debug('Connected to {}:{}...'.format(self.address, self.port))
		self.logger.debug('Connected to {}: {} ...'.format(self.address, self.port))
		# Run handle method and then the finish method
		try:
			self.handle()
		except Exception as e:
			self.logger.exception('Exception: {}', e)
		finally:
			self.finish()

	def finish(self):
		''' Finish connection '''
		self.logger.debug('Finish')
		try:
			self.socket.shutdown(socket.SHUT_RDWR)
		except socket.error:
			pass    # Ignore socket errors
		finally:
			self.socket.close()

# HTTPClient Class

class HTTPClient(TCPClient):
	def _init_(self, url):
		TCPClient._init_(self)
		self.url = url.split('://')[-1]
		if '/' not in url:
			self.path = '/'
		else:
			self.path = '/'+url.split('/', 1)[1]

		if not 'student' in url:
			self.prog = re.compile('(www\..*.com)')
		else:
			self.prog = re.compile('(student..\.cse\.nd\.edu):')
			
		self.host = self.prog.search(url)
		self.host = self.host.group(1)

		self.prog = re.compile(':(\w*)/')
		if self.prog.search(url):
			self.patternMatch = self.prog.search(url)
			self.port = int(self.patternMatch.group(1))
		try:
			self.address = socket.gethostbyname(self.host)
		except socket.gaierror as e:
			logging.error('Unable to lookup {}: {}'.format(self.host, e))

	def handle(self):
		''' Handle connection by reading data and then writing it back until EOF '''
		self.logger.debug('Handle')
		self.stream = self.socket.makefile('w+')
		self.stream.write('GET {} HTTP/1.0\r\nHost: {}\r\n\r\n'.format(self.path, self.host))
		self.stream.flush()
		try:
			data = sys.stdin.readline()
			print data
			while data:
				sys.stdout.write(data)
				data = self.stream.readline()
		except socket.error:
			pass    # Ignore socket errors

# Main Execution

if __name__ == '__main__':
	# Parse command-line arguments
	try:
		options, arguments = getopt.getopt(sys.argv[1:], "hv")
	except getopt.GetoptError as e:
		usage(1)

	for option, value in options:
		if option == '-v':
			LOGLEVEL = logging.DEBUG
		if option == '-r':
			requests = value
		if option == '-p':
			processes = value
		if option == '-h':
			usage(1)
		else:
			usage(1)

	# Set logging level
	logging.basicConfig(
		level   = LOGLEVEL,
		format  = '[%(asctime)s] %(message)s',
		datefmt = '%Y-%m-%d %H:%M:%S',
    	)

	# Lookup host address
	try:
		ADDRESS = socket.gethostbyname(ADDRESS)
	except socket.gaierror as e:
		logging.error('Unable to lookup {}: {}'.format(ADDRESS, e))
		sys.exit(1)

	# Instantiate and run client
	client = HTTPClient(arguments[0])

	try:
		for process in range(int(processes)):
			try:
				pid = os.fork()
				#if child
				if not pid:
					pStart = time.time()
					for requests in range(int(requests)):
						logging.debug('processing requests...')
						client = HTTPClient(arguments[0])
						rStart = time.time()
						client.run()
						rEnd = time.time()
						logging.debug('Elapsed Time {:0.4f} seconds'.format(rEnd - rStart))
					pEnd = time.time()
					avgTime = pEnd - pStart
					avgTime /= int(requests)
					logging.debug('Average Elapsed Time {:0.4f} seconds'.format(avgTime))
					sys.exit(0)
				#if parent	
				else:
					logging.debug('Sending Request ... ')
					logging.debug('Receiving Response ...')
			except OSError as e:
				logging.debug('Could not exec {}'.format(e))
		for process in range(int(processes)):
			pidN, status = os.wait()
			logging.debug('Process {} terminated with an exit status of {}'.format(pidN, status))
	except KeyboardInterrupt:
		sys.exit(0)
