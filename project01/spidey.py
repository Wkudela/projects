#!/usr/bin/env python2.7

import getopt
import logging
import os
import socket
import sys
import re
import mimetypes

# Constants

ADDRESS  = '0.0.0.0'
PORT     = 9234
BACKLOG  = 0
LOGLEVEL = logging.INFO
PROGRAM  = os.path.basename(sys.argv[0])
DOCROOT = 'www'
prog = ''
forking = False

# Utility Functions

def usage(exit_code=0):
    print >>sys.stderr, '''Usage: {program} [-d DOCROOT -p PORT -v -f]

Options:

    -h       Show this help message
    -v       Set logging to DEBUG level
    -f       Enable forking
    -d       set root directory (default is current directory)
    
    -p PORT  TCP Port to listen to (default is {port})
'''.format(port=PORT, program=PROGRAM)
    sys.exit(exit_code)

# BaseHandler Class

class BaseHandler(object):

    def __init__(self, fd, address):
        ''' Construct handler from file descriptor and remote client address '''
        self.logger  = logging.getLogger()        # Grab logging instance
        self.socket  = fd                         # Store socket file descriptor
        self.address = '{}:{}'.format(*address)   # Store address
        self.stream  = self.socket.makefile('w+') # Open file object from file descriptor

        self.debug('Connect')

    def debug(self, message, *args):
        ''' Convenience debugging function '''
        message = message.format(*args)
        self.logger.debug('{} | {}'.format(self.address, message))

    def info(self, message, *args):
        ''' Convenience information function '''
        message = message.format(*args)
        self.logger.info('{} | {}'.format(self.address, message))

    def warn(self, message, *args):
        ''' Convenience warning function '''
        message = message.format(*args)
        self.logger.warn('{} | {}'.format(self.address, message))

    def error(self, message, *args):
        ''' Convenience error function '''
        message = message.format(*args)
        self.logger.error('{} | {}'.format(self.address, message))

    def exception(self, message, *args):
        ''' Convenience exception function '''
        message = message.format(*args)
        self.logger.exception('{} | {}'.format(self.address, message))

    def handle(self):
        ''' Handle connection '''
        self.debug('Handle')
        raise NotImplementedError

    def finish(self):
        ''' Finish connection by flushing stream, shutting down socket, and
        then closing it '''
        self.debug('Finish')
        try:
            self.stream.flush()
            self.socket.shutdown(socket.SHUT_RDWR)
        except socket.error as e:
            pass    # Ignore socket errors
        finally:
            self.socket.close()

# EchoHandler Class

class HTTPHandler(BaseHandler):
    def _init_(self, fd, address, docroot = DOCROOT):
        BaseHandler._init_(self, fd, address)
        self.fd = fd
        self.address = address
        self.docroot = docroot
    def _parse_request(self):
        try:
            data = self.stream.readline()
            request_method = data.strip().split()[1]
            os.environ['REQUEST_METHOD'] = request_method
            request_uri = data.split()[1]
            if '?' in request_uri:
                prog = recompile('(/.*)?(.*)\s')
                request_uri = prog.search(request_uri)
                os.environ['QUERY_STRING'] = request_uri.group(2)
                request_uri = request_uri.group(1)
            os.environ['REQUEST_URI'] = request_uri
            while data:
                self.stream.flush()
                data = self.stream.readline().rstrip()
                if data == '\n':
                    pass
                else:
                    var = data.split(':', 1)[0].upper()
                    if data.split(':')[0] == data.split(':')[0]:
                        value = ''
                    else:
                        value = data.split(':')[0]
                    var = var.split('-')
                    var = ['HTTP'].join(var)
                    var = '_'.join(var)
                    os.environ[var] = value
        except socket.error:
            #ignore socket errors
            pass
        # Init REMOTE_ADDR from address
        var = self.address[0]
        os.environ['REMOTE_ADDR'] = var
    
    def _handle_file(self):
        mimetype, _ = mimetypes.guess_type(self.uripath)
        if mimetype is None:
            mimetype = 'application/octet-stream'
        self.stream.write('<pre>')
        for line in open(self.uripath):
            self.stream.write(line)
        self.stream.write('</pre>')
    def _handle_directory(self):
        def compare(var1, var2):
            if os.path.isdir(var1):
                return 1
            elif os.path.isdir(var2):
                return -1
            else:
                return 0
        self.stream.write("""
            <html lang="en">
            <head>
            <title>/</title>
            <link href="https://www3.nd.edu/~pbui/static/css/blugold.css" rel="stylesheet">
            <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
            </head>
            <body>
            <div class="container">
            <div class="page-header">
            <h2>Directory Listing: {}</h2>
            </div>
            <table class="table table-striped">
            <thead>
            <th>Type</th>
            <th>Name</th>
            <th>Size</th>
            </thead>
            <tbody>
            """.format(os.environ['REQUEST_URI']))
        
        entries = os.listdir(self.uripath)
        sort = sorted(entries, cmp=compare)
        for entry in sort:
            href = os.path.join(os.environ['REQUEST_URI'], entry)
            local = os.path.join(self.uripath, entry)
            if os.path.isdir(entry):
                entry = [href, 'folder', '-', local]
            else:
                entry = [href, 'file', os.path.getsize(localpath), localpath]
            self.stream.write("""
                    <tr>
                <td><i class="fa fa-{}-o"></i></td>
                <td><a href="{}">{}</a></td>
                <td>{}</td>
                </tr>
                """.format(item[1], item[0], item[0], item[2]))
            self.stream.write("""
                    </tbody>
                    </table>
                    </div>
                    </body>
                    </html>
            """)
    def _handle_error(self,error):
        self.stream.write("""
            <html>
            <body>
            <img
            src="https://prod-university-library.s3.amazonaws.com/uploads/pattern/image/2841/10534543634ef1d6658a965.png"
            alt="ERROR {}!!!"
            style="width:750px;height:375px;">
            </body>
            </html>
            """.format(error))
    def _handle_script(self):
        self._parse_request()
        self.uripath = os.path.normpath(self.docroot + os.environ['REQUEST_URI'])
        if self.uripath == '' or not self.uripath.startswith(self.docroot):
            self._handle_error(404)
        elif os.path.isfile(self.uripath) and os.access(self.uripath, os.X_OK):
            self._handle_script()
        elif os.path.isfile(self.uripath) and os.access(self.uripath, os.R_OK):
            self._handle_file()
        elif os.path.isdir(self.uripathi) and os.access(self.uripath, os.R_OK):
            self._handle_directory()
        else:
            self._handle_error(403)

# TCPServer Class

class TCPServer(object):

    def __init__(self, address=ADDRESS, port=PORT, handler=HTTPHandler):
        ''' Construct TCPServer object with the specified address, port, and
        handler '''
        self.logger  = logging.getLogger()                              # Grab logging instance
        self.socket  = socket.socket(socket.AF_INET, socket.SOCK_STREAM)# Allocate TCP socket
        self.address = address                                          # Store address to listen on
        self.port    = port                                             # Store port to lisen on
        self.handler = HTTPHandler                                          # Store handler for incoming connections

    def run(self):
        ''' Run TCP Server on specified address and port by calling the
        specified handler on each incoming connection '''
        try:
            # Bind socket to address and port and then listen
            self.socket.bind((self.address, self.port))
            self.socket.listen()
        except socket.error as e:
            self.logger.error('Could not listen on {}:{}: {}'.format(self.address, self.port, e))
            sys.exit(1)

        self.logger.info('Listening on {}:{}...'.format(self.address, self.port))

        while True:
            # Accept incoming connection
            client, address = self.socket.accept()
            self.logger.debug('Accepted connection from {}:{}'.format(address))
            
            # Instantiate handler, handle connection, finish connection
            if not forking:
                try:
                    handler = self.handler(client, address)
                    handler.handle()
                except Exception as e:
                    handler.exception('Exception: {}', e)
                finally:
                    handler.finish()
            else:
                try:
                    pid = os.fork()
                    #child case
                    if not pid:
                        try:
                            handler = self.handler(client, address)
                            handler.handle()
                            sys.exit(0)
                        except Exception as e:
                            handler.exeption('Exception: {}'.format(e))
                        finally:
                            handler.finish()
                    else:
                        client.close()
                except OSError as e:
                    self.logger.debug('Errof: {}'.format(e))

# Main Execution

if __name__ == '__main__':
    # Parse command-line arguments
    try:
        options, arguments = getopt.getopt(sys.argv[1:], "hp:v")
    except getopt.GetoptError as e:
        usage(1)

    for option, value in options:
        if option == '-p':
            PORT = int(value)
        elif option == '-v':
            LOGLEVEL = logging.DEBUG
        elif option == '-d':
            DOCROOT = value
        elif option == '-f':
            forking = True
        elif option == '-h':
            usage(1)
        else:
            usage(1)

    # Set logging level
    logging.basicConfig(
        level   = LOGLEVEL,
        format  = '[%(asctime)s] %(message)s',
        datefmt = '%Y-%m-%d %H:%M:%S',
    )

    # Instantiate and run server
    server = TCPServer(port=PORT, handler=HTTPHandler)

    try:
        server.run()
    except KeyboardInterrupt:
        sys.exit(0)

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:

